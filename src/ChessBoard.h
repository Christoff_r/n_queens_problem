#pragma once
#include <iostream>
#include <vector>

void makeChessBoard(int size);
void printChessBoard(int size);
void populateChessBoard(int n);