#include "ChessBoard.h"
using namespace std;

//vector<vector<string>> chess_board;
string **chess_board; 

void makeChessBoard(int size)
{
    chess_board = new string *[size];
    
    for (int i = 0; i < size; i++)
    {
        chess_board[i] = new string[size];
    }

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            chess_board[i][j] = "[ ]";
        }
    }
}

void printChessBoard(int size)
{
    cout << "n = " << size << endl;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            cout << chess_board[i][j];
        }
        cout << "\n";
    }
    
    cout << "-------------------------" << endl;
}

void elemenate_fields(int x, int y, int size)
{
    for (int i = 1; i < size; i++)
    {
        int temp_x_pos = x + i;
        int temp_y_pos = y + i;
        
        int temp_x_neg = x - i;
        int temp_y_neg = y - i;
        
        if(temp_y_pos < size)
            chess_board[x][temp_y_pos] = "[X]";
        
        if(temp_x_pos < size)
            chess_board[temp_x_pos][y] = "[X]";

        if (temp_y_pos < size && temp_x_pos < size)
            chess_board[temp_x_pos][temp_y_pos] = "[X]";

        if(temp_y_neg >= 0)
            chess_board[x][temp_y_neg] = "[X]";
        
        if(temp_x_neg >= 0)
            chess_board[temp_x_neg][y] = "[X]";

        if (temp_y_neg >= 0 && temp_x_neg >= 0)
            chess_board[temp_x_neg][temp_y_neg] = "[X]";

        if (temp_y_neg >= 0 && temp_x_pos < size)
            chess_board[temp_x_pos][temp_y_neg] = "[X]";    

        if (temp_y_pos < size && temp_x_neg >= 0)
            chess_board[temp_x_neg][temp_y_pos] = "[X]"; 
    }
    
}

void populateChessBoard(int size)
{
    int placed_queens = 0;

    for (int i = 0; i < size*size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            for (int k = 0; k < size; k++)
            {
                int x = k + (i % size);
                int y = j + (i % (size+ 1));
                if(x < size && y < size && chess_board[y][x] == "[ ]" && placed_queens == 0)
                {
                    chess_board[y][x] = "[Q]";
                    elemenate_fields(y ,x, size);
                    placed_queens++;
                }
                else if (chess_board[j][k] == "[ ]")
                {
                    chess_board[j][k] = "[Q]";
                    elemenate_fields(j ,k, size);
                    placed_queens++;
                }
            }
        }

        if(placed_queens < size)
        {
           placed_queens = 0;
            makeChessBoard(size);
        }else
        {
            printChessBoard(size);
            break;
        }

    }
}
