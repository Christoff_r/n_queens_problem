#include "ChessBoard.h"
using namespace std;

int n_queens = 11; 


int main()
{
    cout << "n = " << n_queens <<endl;

    for (int i = 1; i < n_queens; i++)
    {
        makeChessBoard(i);
        populateChessBoard(i);   
    }

    return 0;
}