# N Queens Problem 

![banner](https://i.gifer.com/79Pw.gif)

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
![language](https://img.shields.io/badge/language-c%2B%2B-blue)
![hello-world](https://img.shields.io/badge/Hello-world-green)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)


## Background

The N Queen Problem is the problem of placing N number of chess queens on a N*N sized chessboard, so that no two queens can take one another, thus no two queens can share the same row, column or diagonal. An eksample can be seen below.

This program uses a brute forces method to find a solution to the problem for 1-10 queens.

![00](https://miro.medium.com/max/457/1*SVCP2lIp1jfzJuQn_QUeVg.png)


The purpose pf this repository is to introduce me to the basics of c++. This include syntax :pencil:, pointers :point_right: (which I still haven’t nailed yet) and .h files.

## Install
Below is a short description on how to install the program. It have no external dependencies or services.

Navigate to were you want the repository to be. Open the `CLI` a type the following.

```
git clone https://gitlab.com/Christoff_r/n_queens_problem.git
```

## Usage
In the repository open the `CLI` and type the following. 
```
cd build
./main
```
If the program finds a solution it will print it out in the `CLI`

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`